import locale

from api import Api
from log import logger
from models import *

class Migration(object):
    """
    High-level migration logic.  Attempts to migrate the requested objects
    from the source to the target.
    """

    def __init__(self, source, target, username, password, cache_file=None):
        self.source_api = Api(source, username, password)
        self.target_api = Api(target, username, password)
        if cache_file:
            cache.load_from_disk(cache_file)
        logger.info('Migration initialized.')

    def migrate_pages(self, label, dry_run=False, force=False):
        """
        Migrate all pages identified by label (either tagged or with a name
        prefix), along with the objects associated with them (recursively).
        """
        try:
            logger.info('migrate_pages(label="%s", dry_run="%s")' %
                             (label, str(dry_run)))
            # Identify pages to be migrated
            logger.info('Identifying pages to migrate.')
            print('Retrieving data to migrate...')
            pages = Page.find_by_label(self.source_api, label)
            for page in pages:
                page.load_members(self.source_api, recurse=True)
            pages_to_migrate = self._show_migration_summary(pages, force)
            if dry_run:
                print('Dry run, nothing migrated.')
            elif pages_to_migrate:
                print('Started migration, see log file for more detail.')
                logger.info('Starting migration...')
                for page in pages_to_migrate:
                    logger.info('Migrating page: %s' % page['name'])
                    page.migrate(self.target_api, force)
                User.validate_subscriptions(self.source_api, self.target_api)
                print('Migration complete.')
            else:
                print('Nothing to do.')
            logger.info('migrate_pages finished.')
        finally:
            cache.dump_to_disk()

    def _filter_existing_objects(self, objects, key='name'):
        "Return objects that don't exist on the target (still need migration)."
        result = []
        for obj in objects:
            if obj.find(self.target_api, obj[key]):
                logger.info('%s %s exists on target.'
                            % (obj.__class__.__name__, obj['name']))
            else:
                result.append(obj)
        return result

    def _find_existing_objects(self, objects, key='name'):
        "Return a list of matching objects that already exist on the target."
        result = []
        for obj in objects:
            target_obj = obj.find(self.target_api, obj[key])
            if target_obj:
                result.append(target_obj)
        return result

    def _show_migration_summary(self, pages, force):
        """
        Display a quick summary of what is about to be migrated.  This is
        not exhaustive, but meant to give the user a sense of what is about to
        happen. (Many kinds of object are not mentioned in the summary.)
        """
        # Compute summary information to display
        users = set()
        lists = set()
        nactions = 0
        pages_to_migrate = pages
        existing_pages = self._find_existing_objects(pages)
        if not force:
            pages_to_migrate = self._filter_existing_objects(pages_to_migrate)
        for page in pages_to_migrate:
            lists.add(page.mailing_list)
            for action in page.actions:
                users.add(action.user)
                nactions += 1
        existing_users = self._find_existing_objects(users, key='email')
        existing_lists = self._find_existing_objects(lists)

        # Format numbers with grouping commas
        fmt = lambda n: locale.format("%d", n, grouping=True)
        print('Migration summary:')
        print('  %s matching pages (%s previously migrated, %s to migrate).' %
              (fmt(len(pages)), fmt(len(existing_pages)),
              fmt(len(pages_to_migrate))))
        print('  %s page actions to migrate.' % fmt(nactions))
        print('  %s associated users (%s previously migrated, %s to migrate).' %
              (fmt(len(users)), fmt(len(existing_users)),
              fmt(len(users) - len(existing_users))))
        print('  %s mailing lists (%s previously migrated, %s to migrate).' %
              (fmt(len(lists)), fmt(len(existing_lists)),
              fmt(len(lists) - len(existing_lists))))
        print('')
        return pages_to_migrate
