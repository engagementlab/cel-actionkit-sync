"""
ActionKit REST API v1 wrapper.

See API documentation here:
https://engagementlab.actionkit.com/docs/manual/api/rest/index.html
"""

import json
from pprint import pformat
import requests
import time

from log import logger
from models import *

RETRY_MAX = 200      # Number of tries to attempt before giving up
RETRY_PERIOD = 60   # Seconds between retries

def call_with_retry(func, *args, **kwargs):
    retries = RETRY_MAX
    while True:
        try:
            return func(*args, **kwargs)
        except requests.exceptions.ConnectionError, e:
            logger.warn('In %s: %s' % (func.func_name, str(e)))
        except requests.exceptions.HTTPError, e:
            logger.warn('In %s: HTTP error %s (%s).\n%s' % (func.func_name,
                                            e.response.status_code,str(e),
                                            )
                        )
        retries -= 1
        if retries:
            logger.warn('Waiting %d seconds to retry.' % RETRY_PERIOD)
            time.sleep(RETRY_PERIOD)
        else:
            logger.error('Retries failed, giving up.')
            raise

class Api(object):
    """
    REST API wrapper.  Generic get methods return the JSON-deserialized
    dictionary of key/values representing the object.  Specific get methods,
    such as find_pages_by_prefix, return model objects like Page.
    """

    HEADERS = {
        'Content-type': 'application/json',
    }

    def __init__(self, domain, username, password):
        "Create an instance for the given domain, e.g. 'engagementlab'."
        self.domain = domain
        self.username = username
        self.password = password
        self.base_url = 'https://act.%s.org' % self.domain
        self.session = requests.session(
            auth=(self.username, self.password),
            config={
                'danger_mode': True, # Raise exception on HTTP errors
            },
            headers=Api.HEADERS,
            timeout=120)

    # Get methods

    def get(self, path, params=None):
        "Perform a GET request on the given path."
        try:
            url = self.base_url + path
        except TypeError,e:
            try:
                url = self.base_url + path()
            except TypeError,e:
                logger.error(e)
                return None
        def do_get():
            try:
                result = self.session.get(url, params=params)
                return json.loads(result.text)
            except ValueError, e:
                message = 'JSON error: %(error)s\nURL: %(url)s\n%(text)s' % {
                    'error': str(e),
                    'text':  result.text,
                    'url':   requests.Request(url, params=params).full_url,
                }
                logger.error(message)
                #raise
            except requests.exceptions.HTTPError, e:
                logger.error('HTTP error in GET: %s\nURL: %s\nparams:%s\n%s' %
                            (str(e), url, params, e.response.text))
                #raise
        return call_with_retry(do_get)

    def get_list(self, path, offset, limit, params=None):
        "Retrieve a list of objects, e.g. /page/."
        if not params:
            params = {}
        params['_offset'] = offset
        params['_limit'] = limit
        result = self.get(path, params)
        return result['objects']

    def get_list_all(self, path, params=None):
        "Get an entire list of objects, handling server-side pagination."
        if not params:
            params = {}
        params['_limit'] = 100
        objects = []
        result = self.get(path, params)
        objects += result['objects']
        while result['meta']['next']:
            result = self.get(result['meta']['next'])
            objects += result['objects']
        return objects

    def get_count(self, path, params=None):
        "Get the number of objects at a particular path."
        if not params:
            params = {}
        params['_limit'] = 1
        result = self.get(path, params)
        return result['meta']['total_count']

    def find_pages_by_tag(self, tag):
        "Find all pages with tagged with the given tag name."
        params = {
            'tags__name': tag,
        }
        return map(Page, self.get_list_all('/rest/v1/page/', params=params))

    def find_pages_by_prefix(self, name_prefix):
        "Find all pages whose names start with the given prefix."
        params = {
            'name__startswith': name_prefix,
        }
        return map(Page, self.get_list_all('/rest/v1/page/', params=params))

    def post(self, path, obj):
        "Post a JSON object to the given path, return the new resource URI."
        url = self.base_url + path
        data = json.dumps(dict(obj))
        logger.debug("POST %s {%s}" % (path,obj.items()))
        def do_post():
            try:
                response = self.session.post(url, data=data)
                path = response.headers['Location']
                path = path[path.find('/rest/'):]
                return path
            except requests.exceptions.HTTPError, e:
                logger.error('HTTP error in POST: %s\nURL: %s\n%s' %
                            (str(e), url, e.response.text))
                logger.info('Object data was:\n%s' % pformat(data))
                #raise
        return call_with_retry(do_post)

    def put(self, obj):
        "PUT a JSON object to its resource_uri, updating the server-side data."
        url = self.base_url + obj['resource_uri']
        data = json.dumps(dict(obj))
        def do_put():
            try:
                self.session.put(url, data=data)
                return
            except requests.exceptions.HTTPError, e:
                logger.error('HTTP error in PUT: %s\nURL: %s\n%s' %
                            (str(e), url, e.response.text))
                logger.info('Object data was:\n%s' % pformat(data))
                #raise
        #return do_put
        return call_with_retry(do_put)

    def delete(self, path):
        "Delete an object from the server."
        try:
            url = self.base_url + path
            self.session.delete(url)
        except requests.exceptions.HTTPError, e:
            logger.error('HTTP error in DELETE: %s\nURL: %s\n%s' %
                         (str(e), url, e.response.text))
            raise

class ApiError(Exception): pass

#solely to pass to cache, just encapsulates the domain
#no connection occurs
class DummyApi(object):
    def __init__(self, domain):
            self.domain = domain