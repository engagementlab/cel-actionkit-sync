from api import Api, DummyApi
from log import logger
from models import *

import jsonpickle
import simplejson
import pprint

class ReportSync(object):
    """
    High-level sync logic
    """

    def __init__(self, instance, username, password, cache_file=None):
        self.instance_api = Api(instance, username, password)
        if not cache_file:
            #make up a new filename based on the instance
            cache_file = '%s_reports.json' % instance
        self.cache_file = cache_file
        logger.info("Report Sync initialized.")

    def dump(self, label):
        logger.info('Dump all reports from %s with label %s' % (self.instance_api,label))
        reports = []
        query_reports = QueryReport.find_by_category(self.instance_api, label)
        dashboard_reports = DashboardReport.find_by_category(self.instance_api, label)
        reports.extend(query_reports)
        reports.extend(dashboard_reports)
        count = 0
        for report in reports:
            ri = report.reindex(self.instance_api)
            count += 1
        logger.info("Dumped %d reports to %s" % (count, self.cache_file))
        cache.dump_to_json(self.cache_file)

    def load(self):
        logger.info('Load reports from %s to %s' % (self.cache_file,self.instance_api.domain))

        #get reports from json cache_file
        cache_content = open(self.cache_file,'r').read()
        cache_data = jsonpickle.decode(cache_content)

        #cache data is in odd format
        #pull out of triple-nested dict, as per ModelCache comments
        reports = {}
        domains = []
        for domain in cache_data.keys():
            r = cache_data[domain][str(Report)]
            reports.update(r)
            domains.append(domain)
            logger.info('loaded cached reports from %s' % domain)
        #print reports

        for (key,report) in reports.items():
            if not 'short_name' in report:
                #don't have the necessary info, ignore it
                continue
            logger.info('loading report %s' % report['short_name'])
            report.migrate(self.instance_api)

