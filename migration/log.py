"""
Provides log object used by the migration classes.

Creates a directory called 'logs' in the current working directory (if it
doesn't already exist) and then writes a log file into that directory.
"""

import datetime
import logging
import os

LOG_FILE = 'logs/migration-%Y-%m-%d-%H-%M-%S.log'

def _init_logger():
    if not os.path.exists('logs'):
        os.mkdir('logs')
    logger = logging.getLogger(__name__)
    filename = datetime.datetime.now().strftime(LOG_FILE).format()
    hdlr = logging.FileHandler(filename)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger

def init_console():
    # create console handler with a higher log level
    logger = logging.getLogger(__name__)
    hdlr = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    hdlr.setLevel(logging.INFO)

# Global logger object
logger = _init_logger()
