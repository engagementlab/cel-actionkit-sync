"""
Data models representing objects to be migrated.  The object data is kept in
the JSON-parsed form (i.e. a dictionary of key/value pairs) except where
needed to store relationship information used during migration.

Note 1: These models use caching to avoid repeated GET calls on the server.
This means that changes that occur during the migration may be missed.  The
caches are indexed by API instance, so as long as you keep passing the same
API object, you'll get the same model objects back.

Because of the cache, migration code can be kept simple and lazy.  Objects
are migrated as needed, rather than predicting everything in advance.

Note 2: The identifier 'mailing_list' is used since 'list' is a keyword in
Python.
"""

import collections
import copy
from datetime import datetime
import hashlib
import pickle
import jsonpickle
import re
import requests

from log import logger
import traceback

ACTION_TIMESTAMP_FORMAT = '%Y-%m-%dT%H:%M:%S'

class ModelCache(object):
    def __init__(self):
        # Triple-nested: subdomain -> class -> key: object
        # {
        #     'my_domain': {   (e.g. "engagementlab")
        #         'ModelClass': {
        #             'unique_id1': model_object_instance1,
        #             'unique_id2': model_object_instance2,
        #         }
        #     }
        # }
        self.data = {}

    def load_from_disk(self, path):
        "Replace the cache data with the contents of the given .dat file."
        self.data = pickle.load(open(path, 'rb'))

    def dump_to_disk(self):
        pickle.dump(self.data, open('cache.dat', 'wb'), 2)

    def load_from_json(self,path):
        "Replace the cache data with the contents of the given json file."
        self.data = jsonpickle.decode(open(path,'rb').readlines())

    def dump_to_json(self, path):
        "Dump to a json cache file, with indentation so it's readable and diff'able"
        jsonpickle.set_preferred_backend('simplejson')
        jsonpickle.set_encoder_options('simplejson', indent=4)

        json_data = jsonpickle.encode(self.data)
        outfile = open(path,'wb')
        outfile.writelines(json_data)

    def find(self, api, klass, key):
        "Return the matching cached object or None if not found."
        domain_cache = self.data.setdefault(api.domain, {})
        class_cache = domain_cache.setdefault(klass, {})
        return class_cache.get(key)

    def find_class(self, api, klass):
        "Return all matching cached objects for a given class"
        domain_cache = self.data.setdefault(api.domain, {})
        class_cache = domain_cache.setdefault(klass, {})
        return class_cache.values()

    def find_or_update(self, api, obj):
        "Return the cached object matching obj, or store obj if not found."
        domain_cache = self.data.setdefault(api.domain, {})
        class_cache = domain_cache.setdefault(obj.__class__, {})
        cached_obj = class_cache.get(obj['resource_uri'])
        key = obj.__class__.unique_key
        if key:
            # Sanity check: Same object should be stored for both keys
            cached_obj2 = class_cache.get(obj[key])
            assert cached_obj == cached_obj2
        if cached_obj:
            return cached_obj
        self.store(api, obj)
        return obj

    def invalidate(self, api, obj):
        "Invalidate the given cached object so it will be fetched next get()."
        domain_cache = self.data.setdefault(api.domain, {})
        class_cache = domain_cache.setdefault(obj.__class__, {})
        del class_cache[obj['resource_uri']]
        key = obj.__class__.unique_key
        if key:
            del class_cache[obj[key]]

    def store(self, api, obj):
        "Store the given object in the cache."
        domain_cache = self.data.setdefault(api.domain, {})
        class_cache = domain_cache.setdefault(obj.__class__, {})
        class_cache[obj['resource_uri']] = obj
        key = obj.__class__.unique_key
        if key:
            class_cache[obj[key]] = obj

    def reset(self):
        "Wipe the cache clean."
        self.data = {}

# Global cache
cache = ModelCache()

# Note: As with caching, the migration logic really depends on this decorator
# to allow the migration code to be lazy and migrate objects on demand.  So
# this isn't so much an optimization as it is a requirement for proper
# functioning.
def memoized_migration(func):
    """
    This decorator allows migrate methods to remember the result for
    subsequent calls and not re-update any data on the target server.
    (Just return the object that resulted from the last migrate call.)
    """
    def wrapped(self, *args, **kwargs):
        if self.migrated:
            return self.migrated
        else:
            self.migrated = func(self, *args, **kwargs)
            return self.migrated
    return wrapped

class BaseModel(collections.MutableMapping):
    """
    Base class for models that allows the object dictionary to be accessed
    directly as if the model is a dictionary, while having additional data
    members available.

    This class also provides generic get() and find() methods which use the
    cache and keep it up to date, so all models will have these operations.
    """

    # The base URL on the server for objects of this type, e.g. '/rest/v1/foo/'
    base_path = None

    # If this object has a field that uniquely identifies it (e.g. 'name'),
    # then set unique_key to this field name.  Objects that have a unique_key
    # will support find() and migrate().
    unique_key = None

    # If the API allows for this object to be found by searching on a key,
    # then set this value to the name of the search parameter (e.g. 'email').
    # This will make find() more efficient.  find() will work even if you do
    # not define this value, but it will fetch ALL objects and look for the
    # matching object manually instead.
    find_param = None

    def __init__(self, obj):
        self.migrated = None
        if obj:
            self.store = dict(obj)
        else:
            self.store = {}

    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        self.store[key] = value

    def __delitem__(self, key):
        del self.store[key]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __hash__(self):
        return id(self)

    def __unicode__(self):
        return self['resource_uri']

    @classmethod
    def get(klass, api, path, recurse=False):
        "Get an object from the given path."
        obj = cache.find(api, klass, path)
        if not obj:
            obj = klass(api.get(path))
            obj.post_get_hook(api, path)
            cache.store(api, obj)
            if recurse:
                obj.load_members(api, recurse)
        return obj

    @classmethod
    def get_list(klass, api, path, recurse=False):
        "Get a list of objects from the given path."
        result = []
        objects = map(klass, api.get_list_all(path))
        for obj in objects:
            obj.post_get_hook(api, path)
            result.append(cache.find_or_update(api, obj))
            if recurse:
                obj.load_members(api, recurse)
        return result

    @classmethod
    def find(klass, api, value, recurse=False):
        "Find a model object by its unique identifier, or None if not found."
        if not klass.unique_key or not klass.base_path:
            raise NotImplementedError('find() is not implemented for %s' %
                                      klass.__name__)
        obj = cache.find(api, klass, value)
        if obj:
            return obj
        if klass.find_param:
            # Not cached, perform a search if search is supported
            params = { klass.find_param: value }
            result = api.get_list_all(klass.base_path, params)
            if not result:
                return None
            assert len(result) == 1 # This is supposed to be a unique ID
            obj = klass(result[0])
            obj.post_get_hook(api, klass.base_path)
            cache.store(api, obj)
        if not obj:
            # API doesn't support searching, so fetch all objects and search
            objects = map(klass, api.get_list_all(klass.base_path))
            for o in objects:
                o.post_get_hook(api, klass.base_path)
                cache.find_or_update(api, o)
            obj = cache.find(api, klass, value)
        if obj and recurse:
            obj.load_members(api, True)
        return obj

    def load_members(self, api, recurse=True):
        "Load any related objects that this object refers to."
        pass

    @memoized_migration
    def migrate(self, api):
        "Migrate method for simple objects."
        klass = self.__class__
        if not klass.unique_key:
            raise NotImplementedError('migrate() is not implemented for %s' %
                                      klass.__name__)
        # Look for an existing object first
        new_object = klass.find(api, self[klass.unique_key])
        if not new_object:
            # If it does not exist, migrate
            new_object = self.reindex(api)
            path = api.post(self.__class__.base_path, new_object)
            new_object = self.__class__.get(api, path)
        return new_object

    def reindex(self, api):
        "Reindex method for simple objects."
        # Returns a copy of this object with resource_uri removed.
        result = copy.copy(self)
        result.store = copy.deepcopy(self.store)
        if 'resource_uri' in result:
            del result['resource_uri']
        if 'id' in result:
            del result['id']
        return result

    def show_partial_migration_warning(self, api):
        print('-------------------------------------------------------')
        print('WARNING: Object partially migrated: %s' % self['resource_uri'])
        print('You may want to delete this object so it will be migrated')
        print('completely next time, e.g.:')
        print('')
        print('  delete_request.py -i %s -u USERNAME -p PASSWORD %s' %
              (api.domain, self['resource_uri']))
        print('-------------------------------------------------------')

    def post_get_hook(self, api, path):
        "Called after object(s) are retrieved with GET using the given path."
        pass

class Action(BaseModel):
    """
    Actions are unfortunately interesting.  The POST interface for creating
    them is quite different from everything else.  Read the documentation
    here:

    https://engagementlab.actionkit.com/docs/manual/api/rest/actionprocessing.html

    As a result, several items in the original action have to be transformed
    before POSTing so that they are migrated correctly.  (User -> email,
    page -> page name, opt_in set based on subscribed_user, referring user
    token -> referring_akid.)
    """
    base_path = '/rest/v1/action/'

    def __init__(self, obj):
        super(Action, self).__init__(obj)
        self.page = None
        self.referring_user = None
        self.user = None
        self.created_at = None

    def load_members(self, api, recurse=True):
        "Retrieve associated user from the server and store in this object."
        if not self.page:
            self.page = Page.get(api, self['page'], recurse=True)
        if not self.referring_user and self['referring_user']:
            self.referring_user = User.get(
                api, self['referring_user'], recurse=True)
        if not self.user:
            self.user = User.get(api, self['user'], recurse=True)
        if not self.created_at:
            #get best guess at source action
            #this is ugly
            source_action = api.get('/rest/v1/action/',{'page': self.page['id'],'user':self.user['id']})
            try:
                source_created_at = source_action['objects'][0]['created_at']
                self.created_at = source_created_at
            except KeyError,e:
                logger.error("Can't match source created_at for %s, %s" % (self.user['resource_uri'],self.page['resource_uri']))

    @memoized_migration
    def migrate(self, api):
        "Naive migrate: always creates a new action."
        action = self.reindex(api)
        path = api.post(Action.base_path, action)
        if action.user and action['subscribed_user']:
            # Update our local copy so we know this user got subscribed
            action.user['subscription_status'] = 'subscribed'
        return Action.get(api, path)

    def reindex(self, api):
        result = super(Action, self).reindex(api)
        del result['akid']
        # Fields must be formatted as action_foo: value
        del result['fields']
        if self['fields']:
            for name, value in self['fields'].iteritems():
                result['action_%s' % name] = value
        # Page should be migrated before actions, so we should always find it:
        result.page = Page.find(api, self.page['name'])
        assert result.page
        # API expects page name and user email (even though these fields are
        # normally shown by resource_uri)
        result['page'] = result.page['name']
        if self.referring_user:
            result.referring_user = self.referring_user.migrate(api)
            result['referring_akid'] = result.referring_user['token']
        result.user = self.user.migrate(api)
        result['email'] = result.user['email']
        result['created_at'] = self.created_at
        del result['user']

        if result.page['type'] == "Call":
            #for calls, convert 'targeted' and 'checked' to 'target_checked'
            #and lookup id on new instance
            if self['status'] == "complete":
                if self['checked']:
                    source_target_id = int(self['checked'][0].split('/')[-2]) #-2 because of trailing slash
                elif self['targeted']:
                    #assume they meant to check first target
                    source_target_id = int(self['targeted'][0].split('/')[-2]) #-2 because of trailing slash
                else:
                    source_target_id = None
               
                if source_target_id:
                    try:
                        new_target_id = Target.target_map.get(source_target_id) #get new target from map
                        #target = Target.get(api,'/rest/v1/target/%d/' % new_target_id) #full target
                        result['target_checked'] = [new_target_id,] #only actually requires id
                    except (AttributeError,ValueError,TypeError):
                        logger.error("Target %s->%s" % (source_target_id,new_target_id))
                        logger.error("unable to find Target %s on %s" % (self['targeted'],api.domain))
                else:
                    result['target_checked'] = []

                #pass address1 to action, because of targeting
                if result.user['address1']:
                    result['address1'] = result.user['address1']
                result['nojs'] = 1 # to force ak to accept target_checked (work around generic action processing)
            del result['targeted']
            del result['checked']

        if not result['subscribed_user']:
            # If opt_in is not present, then the user will be subscribed
            # to the related page list.
            result['opt_in'] = True
        return result

class AllowedPageField(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/allowedpagefield/'

    def __init__(self, obj):
        super(AllowedPageField, self).__init__(obj)

    @classmethod
    def find(klass, api, value, recurse=False):
        "AllowedPageField path is its name, so find can be short-circuited."
        try:
            path = AllowedPageField.base_path + value + '/'
            return AllowedPageField.get(api, path, recurse)
        except requests.exceptions.HTTPError, e:
            if e.response.status_code == 404:
                return None
            raise

class AllowedUserField(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/alloweduserfield/'

    def __init__(self, obj):
        super(AllowedUserField, self).__init__(obj)

    @classmethod
    def find(klass, api, value, recurse=False):
        "AllowedUserField path is its name, so find can be short-circuited."
        try:
            path = AllowedUserField.base_path + value + '/'
            return AllowedUserField.get(api, path, recurse)
        except requests.exceptions.HTTPError, e:
            if e.response.status_code == 404:
                return None
            raise

class EmailWrapper(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/emailwrapper/'

    def __init__(self, obj):
        super(EmailWrapper, self).__init__(obj)
        self.language = None

    def load_members(self, api, recurse=True):
        if not self.language:
            self.language = Language.get(api, self['lang'])

class Form(BaseModel):
    # There are many kinds of form, so no single base_url here.  Forms have a
    # 1-to-1 relationship with pages.

    def __init__(self, obj):
        super(Form, self).__init__(obj)
        self.page = None
        self.templateset = None

    def load_members(self, api, recurse=True):
        if not self.page:
            self.page = Page.get(api, self['page'], recurse)
        if not self.templateset:
            self.templateset = TemplateSet.get(
                api, self['templateset'], recurse)

    @memoized_migration
    def migrate(self, api, new_form=None):
        """
        If new_form exists, then it will be updated with the migrated data
        rather than created anew.
        """
        if new_form:
            path = new_form['resource_uri']
            form = self.reindex(api)
            form['resource_uri'] = path
            api.put(form)
            cache.invalidate(api, new_form)
            logger.debug('Updated form at: %s' % path)
        else:
            path = re.sub(r'(.+)/(.+form)/.+', '\\1/\\2/', self['resource_uri'])
            path = api.post(path, self.reindex(api))
            logger.debug('Created form at: %s' % path)
        return Form.get(api, path)

    def reindex(self, api):
        result = super(Form, self).reindex(api)
        # Page should be migrated before form, so we should always find it:
        result.page = Page.find(api, self.page['name'])
        assert result.page
        result['page'] = result.page['resource_uri']
        result.templateset = self.templateset.migrate(api)
        result['templateset'] = result.templateset['resource_uri']
        if 'surveyquestions' in result:
            for q in result['surveyquestions']:
                del q['survey_form']
        return result

class FromLine(BaseModel):
    unique_key = 'from_line'
    base_path = '/rest/v1/fromline/'

    def __init__(self, obj):
        super(FromLine, self).__init__(obj)

class HostingPlatform(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/hostingplatform/'

    def __init__(self, obj):
        super(HostingPlatform, self).__init__(obj)

class Language(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/language/'
    find_param = 'name'

    def __init__(self, obj):
        super(Language, self).__init__(obj)

class List(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/list/'

    def __init__(self, obj):
        super(List, self).__init__(obj)

class Page(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/page/'
    find_param = 'name'

    # Most pages have a form ('cms_form'), this is handled generically.
    supported_types = [
        'Call',        # Fields: target_groups
        'Import',      # No special fields
        'Petition',    # No special fields
        'Signup',      # No special fields
        'Survey',      # No special fields
        'Unsubscribe', # No special fields
        'Letter',      # TBD
        'LTE',         # special fields: canned_letter
        'Donation',    # special fields: products, amount
        'Whipcount',   # special fields?
    ]

    # Note: Pages in ActionKit cannot be deleted, period. They can only be
    # hidden, but even hidden pages will cause name conflicts if you try to
    # create a new page with the same name.
    #
    # Therefore, migrate() assumes that pages always need migration, even if
    # they already exist.  It will look at the related fields (such as
    # actions, followup, etc.), and if they have not been migrated, it will
    # attempt to migrate them.  Templates are always updated for the same
    # reason.

    def __init__(self, obj):
        super(Page, self).__init__(obj)
        self.actions = []
        self.fields = []
        self.followup = None
        self.form = None
        self.hosted_with = None
        self.language = None
        self.mailing_list = None
        self.tags = []
        self.target_groups = []
        if self['type'] not in Page.supported_types:
            raise Exception("Unsupported page type: %s" % self['type'])

    @staticmethod
    def find_by_label(api, label):
        "Return a list of pages either prefixed with or tagged with label."
        # Get tagged or prefixed pages
        tpages = api.find_pages_by_tag(label)
        ppages = api.find_pages_by_prefix(label)
        # Merge the two lists (using name as the unique id)
        d = {}
        for page in tpages + ppages:
            d[page['name']] = page
        pages = []
        for page in d.values():
            page.post_get_hook(api, Page.base_path)
            pages.append(cache.find_or_update(api, page))
        logger.info('Found %d pages matching %s.' % (len(pages), label))
        return pages

    def load_members(self, api, recurse=True):
        "Retrieve associated actions from the server and store in this object."
        if not self.actions:
            count = api.get_count(self['actions'])
            if count:
                logger.debug('%s: Page %s: loading %d actions.' %
                             (api.domain, self['name'], count))
            self.actions = Action.get_list(api, self['actions'], recurse)
            logger.debug('%s: Page %s: got %d actions.' %
                             (api.domain, self['name'], len(self.actions)))
        if not self.fields:
            for name, value in self['fields'].iteritems():
                self.fields.append(AllowedPageField.find(api, name, recurse))
        followup_path = None
        if 'followup' in self and self['followup']:
            followup_path = self['followup']['resource_uri']
        if not self.followup and followup_path:
            self.followup = PageFollowup.get(api, followup_path, recurse)
        if not self.form and 'cms_form' in self:
            self.form = Form.get(api, self['cms_form'], recurse)
        if not self.hosted_with:
            self.hosted_with = HostingPlatform.get(api, self['hosted_with'])
        if not self.language:
            self.language = Language.get(api, self['language'])
        if not self.mailing_list:
            self.mailing_list = List.get(api, self['list'])
        if not self.tags:
            for tag in self['tags']:
                self.tags.append(Tag.get(api, tag['resource_uri']))
        if self['type'] == 'Call':
            if not self.target_groups:
                for group in self['target_groups']:
                    group = TargetGroup.get(api, group['resource_uri'], recurse)
                    self.target_groups.append(group)
        #TODO, more donation checking here
        if self['type'] == 'Donation':
            self.donation_amount = Page.get(api, 'amount')

        #temp, set required fields to none, so we can migrate all actions
        self['required_fields'] = []

    @memoized_migration
    def migrate(self, api, force=False):
        "Migrate this page (and dependencies) to the given API object's server."
        logger.debug('Migrating page "%s".' % self['name'])
        page = Page.find(api, self['name'], recurse=True)
        if page:
            # Migrate any member fields that weren't previously migrated
            path = page['resource_uri']
            new_page = self.reindex(api)
            new_page['resource_uri'] = path
            api.put(new_page)
            cache.invalidate(api, page)
            logger.debug('Updated page at: %s' % path)
        else:
            path = re.sub(r'(.+)/(.+page)/.+', '\\1/\\2/', self['resource_uri'])
            path = api.post(path, self.reindex(api))
            logger.debug('Created page at: %s' % path)
        new_page = Page.get(api, path, recurse=True)
        try:
            if not new_page.followup and self.followup:
                new_page.followup = self.followup.migrate(api)
                new_page['followup'] = dict(new_page.followup)
            if self.form:
                new_page.form = self.form.migrate(api, new_page.form)
                new_page['cms_form'] = new_page.form['resource_uri']
            latest_timestamp = None
            for action in new_page.actions:
                timestamp = datetime.strptime(action['created_at'],
                                              ACTION_TIMESTAMP_FORMAT)
                if not latest_timestamp or timestamp > latest_timestamp:
                    latest_timestamp = timestamp
            if force:
                logger.info('Force, migrate all page actions.')
                latest_timestamp = None
                #remigrate all actions, not just the latest
            if latest_timestamp:
                logger.info('Migrating actions newer than %s' %
                            str(latest_timestamp))
            else:
                logger.info('Migrating all %s page actions.' % len(self.actions))

            #check if email is enabled, so we can reset after migration
            email_enabled = new_page.followup and \
                            new_page.followup['send_email']
            if email_enabled:
                new_page.followup.enable_email(api, False)
            
            action_count = 0
            try:
                for action in self.actions:
                    migrate = True
                    if latest_timestamp:
                        t = datetime.strptime(action['created_at'],
                                              ACTION_TIMESTAMP_FORMAT)
                        if t <= latest_timestamp:
                            migrate = False
                    if migrate:
                        action_count += 1
                        try:
                            new_page.actions.append(action.migrate(api))
                        except Exception,e:
                            logger.error("Exception in page migration, continuing")
                            logger.error(traceback.format_exc())

                            continue
                logger.info('Migrated %d action(s).' % action_count)
            finally:
                if email_enabled and new_page.followup:
                    new_page.followup.enable_email(api, True)
        except Exception,e:
            print "Exception in page migration",traceback.format_exc()
            new_page.show_partial_migration_warning(api)
            logger.error("Exception in page migration"+str(e))
            #raise
        return new_page

    def reindex(self, api):
        """
        Return a new Page object with updated resource references such that
        they are valid on the given API server.  If a mapping is unable to be
        completed, will throw an exception.  Values that are no longer
        appropriate (such as resource_uri) will be unset.
        """
        result = super(Page, self).reindex(api)
        del result['actions']
        result.fields = []
        for field in self.fields:
            result.fields.append(field.migrate(api))
        if 'cms_form' in result:
            result.form = None
            del result['cms_form']
        if 'followup' in result:
            result['followup'] = None
        result.hosted_with = self.hosted_with.migrate(api)
        result['hosted_with'] = result.hosted_with['resource_uri']
        result.language = self.language.migrate(api)
        result['language'] = result.language['resource_uri']
        result.mailing_list = self.mailing_list.migrate(api)
        result['list'] = result.mailing_list['resource_uri']
        result.tags = []
        result['tags'] = []
        for tag in self.tags:
            new_tag = tag.migrate(api)
            result.tags.append(new_tag)
            result['tags'].append(new_tag['resource_uri'])
        if result['type'] == 'Call':
            result.target_groups = []
            result['target_groups'] = []
            for group in self.target_groups:
                new_group = group.migrate(api)
                result.target_groups.append(new_group)
                result['target_groups'].append(new_group['resource_uri'])
        return result

    def post_get_hook(self, api, path):
        """
        Pages come in several kinds and behave differently if they are fetched
        with /page/X or /kindpage/X.  In the latter form, more attributes are
        returned, specific to the kind of page it is.  Therefore, every time a
        page is retrieved with the generic /page/ form, we need to update it
        with the fields retrieved from the page's canonical resource_uri.
        """
        if path != self['resource_uri']:
            logger.debug('Fetching additional page info: %s' %
                         self['resource_uri'])
            obj = api.get(self['resource_uri'])
            self.update(obj)

class PageFollowup(BaseModel):
    base_path = '/rest/v1/pagefollowup/'

    # PageFollowups have a 1-to-1 relationship with Pages

    def __init__(self, obj):
        super(PageFollowup, self).__init__(obj)
        self.email_wrapper = None
        self.from_line = None
        self.page = None

    def enable_email(self, api, enabled):
        "Set the send_email value True or False for this page followup."
        logger.info('%sabling email for %s.' %
                    ('En' if enabled else 'Dis', self['resource_uri']))
        self['send_email'] = enabled
        api.put(self)

    def load_members(self, api, recurse=True):
        if not self.email_wrapper and 'email_wrapper' in self:
            path = self['email_wrapper']
            if path:
                self.email_wrapper = EmailWrapper.get(api, path, recurse)
        if not self.from_line and 'email_from_line' in self:
            path = self['email_from_line']
            if path:
                self.from_line = FromLine.get(api, path, recurse)
        if not self.page:
            self.page = Page.get(api, self['page'])

    @memoized_migration
    def migrate(self, api):
        "Naive migrate: always creates a new followup."
        path = api.post(PageFollowup.base_path, self.reindex(api))
        return PageFollowup.get(api, path, True)

    def reindex(self, api):
        result = super(PageFollowup, self).reindex(api)
        assert self.page
        # Page should be migrated before form, so we should always find it:
        result.page = Page.find(api, self.page['name'])
        assert result.page
        result['page'] = result.page['resource_uri']
        if result.email_wrapper:
            result.email_wrapper = self.email_wrapper.migrate(api)
            result['email_wrapper'] = result.email_wrapper['resource_uri']
        if result.from_line:
            result.from_line = self.from_line.migrate(api)
            result['email_from_line'] = result.from_line['resource_uri']
        return result

class Phone(BaseModel):
    base_path = '/rest/v1/phone/'

    def __init__(self, obj):
        super(Phone, self).__init__(obj)

    @memoized_migration
    def migrate(self, api):
        "Naive migrate: always creates a new phone number."
        path = api.post(Phone.base_path, self.reindex(api))
        return Phone.get(api, path)

    def reindex(self, api):
        result = super(Phone, self).reindex(api)
        result.user = self.user.migrate(api)
        result['user'] = result.user['resource_uri']
        return result

class Subscription(BaseModel):
    base_path = '/rest/v1/subscription/'

    def __init__(self, obj):
        "Create a subscription from obj, List object not populated by default."
        super(Subscription, self).__init__(obj)
        self.mailing_list = None
        self.user = None

    def load_members(self, api, recurse=True):
        "Retrieve the associated mailing list and user for this object."
        if not self.mailing_list:
            self.mailing_list = List.get(api, self['list'])
        if not self.user:
            self.user = User.get(api, self['user'], recurse)

class Tag(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/tag/'
    find_param = 'name'

    def __init__(self, obj):
        super(Tag, self).__init__(obj)

class Target(BaseModel):
    base_path = '/rest/v1/target/'

    # These all map to CongressTarget
    congress_types = [
        'delegate',
        'house',
        'senate',
        'state_house',
        'state_senate',
    ]

    supported_types = congress_types + [
        # SpecialTarget (sometimes)
        'other',
    ]

    # Unfortunately, type 'other' usually means SpecialTarget, but not always.
    # You'll see some defensive coding in here to handle that case.

    # Maps IDs from source server to target server
    target_map = {}
    target_map_file = ''

    @staticmethod
    def build_target_map(source_api, target_api):
        # Unfortunate name collision - target_api is the server we're
        # migrating to, nothing to do with the Target objects.
        count = source_api.get_count(Target.base_path)
        logger.info('Fetching %d targets from %s.' % (count, source_api.domain))
        source_targets = Target.get_list(source_api, Target.base_path)
        count = target_api.get_count(Target.base_path)
        logger.info('Fetching %d targets from %s.' % (count, target_api.domain))
        target_targets = Target.get_list(target_api, Target.base_path)
        logger.info('Building target map...')
        for s in source_targets:
            if s['id'] in Target.target_map:
                continue
            for t in target_targets:
                if s['type'] == t['type']:
                    if s['type'] in ['senate', 'state_senate']:
                        if s['seat'] == t['seat'] and \
                           s['full_name'] == t['full_name']:
                            Target.target_map[s['id']] = t['id']
                            break
                    elif s['type'] in ['house', 'state_house']:
                        if s['us_district'] == t['us_district'] \
                           and s['full_name'] == t['full_name']:
                            Target.target_map[s['id']] = t['id']
                            break
                    elif s['type'] == 'other':
                        if s['title_full'] == t['title_full']:
                            Target.target_map[s['id']] = t['id']
                            break
        for t in target_targets:
            sources = []
            for s in source_targets:
                if Target.target_map.get(s['id']) == t['id']:
                    sources.append(s)
            if len(sources) > 1:
                source_ids = map(lambda s: s['id'], sources)
                source_ids = map(str, source_ids)
                logger.warn('Multiple sources map to targets: (%s) -> %d.' %
                            (', '.join(source_ids), t['id']))
        logger.info('Mapped %d targets (out of %d and %d).' %
                    (len(Target.target_map), len(source_targets),
                    len(target_targets)))

    @staticmethod
    def load_target_map():
        Target.target_map = pickle.load(open(Target.target_map_file, 'rb'))

    @staticmethod
    def save_target_map():
        pickle.dump(Target.target_map, open(Target.target_map_file, 'wb'), 2)

    def __init__(self, obj):
        super(Target, self).__init__(obj)
        if self['type'] not in Target.supported_types:
            raise Exception('Unsupported target type, "%s", in target %d' %
                            (self['type'], self['id']))
        if self['type'] == 'other':
            self.body = None
            self.target_contacts = []
            self.target_offices = []

    def load_members(self, api, recurse=True):
        if self['type'] == 'other':
            if not self.body and 'body' in self:
                self.body = TargetGroup.get(api, self['body'], recurse)
            if not self.target_contacts and 'targetcontacts' in self:
                for path in self['targetcontacts']:
                    contact = TargetContact.get(api, path, recurse)
                    self.target_contacts.append(contact)
            if not self.target_offices and 'targetoffices' in self:
                for path in self['targetoffices']:
                    office = TargetOffice.get(api, path, recurse)
                    self.target_offices.append(office)

    @memoized_migration
    def migrate(self, api):
        if self['id'] in Target.target_map:
            new_id = Target.target_map[self['id']]
            path = '%s%d/' % (Target.base_path, new_id)
            result = Target.get(api, path, True)
        else:
            logger.debug('Target %d needs migration.' % self['id'])
            if self['type'] in Target.congress_types:
                base_path = '/rest/v1/target/'
            else:
                base_path = '/rest/v1/specialtarget/'
            path = api.post(base_path, self.reindex(api))
            result = Target.get(api, path, True)
            Target.target_map[self['id']] = result['id']
            Target.save_target_map()
        return result

    def reindex(self, api):
        result = super(Target, self).reindex(api)
        if result['type'] == 'other':
            if self.body:
                result.body = self.body.migrate(api)
                result['body'] = result.body['resource_uri']
            result.target_contacts = []
            result['targetcontacts'] = []
            for contact in self.target_contacts:
                new_contact = contact.migrate(api)
                result.target_contacts.append(new_contact)
                result['target_contacts'] = new_contact['resource_uri']
            result.target_offices = []
            result['targetoffices'] = []
            for office in self.target_offices:
                new_office = office.migrate(api)
                result.target_offices.append(new_office)
                result['target_offices'] = new_office['resource_uri']
        return result

    def post_get_hook(self, api, path):
        if '/target/' in path:
            try:
                if self['type'] == 'other':
                    obj = api.get('/rest/v1/specialtarget/%d/' % self['id'])
                    self.update(obj)
            except (requests.exceptions.HTTPError, TypeError):
                logger.info(('Target %d is not a SpecialTarget despite ' +
                            '"other" type.') % self['id'])

class TargetGroup(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/targetgroup/'
    find_param = 'name'

    # These all map to CongressTargetGroup
    congress_types = [
        'house',
        'senate',
        'state_house',
        'state_senate',
    ]

    supported_types = congress_types + [
        # SpecialTargetGroup
        'other',
    ]

    def __init__(self, obj):
        super(TargetGroup, self).__init__(obj)
        self.targets = []
        self.all_targets = []
        if self['type'] in TargetGroup.congress_types:
            self.targets_field = 'targets'
        elif self['type'] == 'other':
            self.targets_field = 'specialtargets'
        else:
            raise Exception('Unsupported TargetGroup type: %s' % self['type'])

    def load_members(self, api, recurse=True):
        if not self.targets:
            for target_path in self[self.targets_field]:
                self.targets.append(Target.get(api, target_path, recurse))
        if not self.all_targets:
            self.all_targets = Target.get_list(api, self['all'], True)

    def migrate(self, api):
        result = TargetGroup.find(api, self['name'])
        if not result:
            result = self.reindex(api)
            if self['type'] in TargetGroup.congress_types:
                base_path = '/rest/v1/congresstargetgroup/'
            elif self['type'] == 'other':
                base_path = '/rest/v1/specialtargetgroup/'
            path = api.post(base_path, result)
            result = TargetGroup.get(api, path, True)
            # Ensure all targets in this group have been migrated
            result[result.targets_field] = []
            result.targets = []
            for target in self.targets:
                new_target = target.migrate(api)
                result.targets.append(new_target)
                result[result.targets_field].append(new_target['resource_uri'])
            for target in self.all_targets:
                target.migrate(api)
        return result

    def reindex(self, api):
        result = super(TargetGroup, self).reindex(api)
        if 'all' in result:
            del result['all']
        result[result.targets_field] = []
        return result

    def post_get_hook(self, api, path):
        """
        Like Pages, TargetGroups come in several kinds and behave differently
        if they are fetched with /targetgroup/X or /targetgroup/X.  In the
        latter form, more attributes are returned, specific to the kind of
        group it is.  Therefore, every time a targetgroup is retrieved with
        the generic /targetgroup/ form, we need to update it with the fields
        retrieved from the group's canonical resource_uri.
        """
        if '/targetgroup/' in path:
            if self['type'] in TargetGroup.congress_types:
                obj = api.get('/rest/v1/congresstargetgroup/%d/' % self['id'])
            elif self['type'] == 'other':
                obj = api.get('/rest/v1/specialtargetgroup/%d/' % self['id'])
            else:
                raise Exception('Unsupported TargetGroup type: %s'
                                % self['type'])
            self.update(obj)

class TargetContact(BaseModel):
    base_path = '/rest/v1/targetcontact/'

    def __init__(self, obj):
        super(TargetContact, self).__init__(obj)
        self.target = None

    def load_members(self, api, recurse=True):
        if not self.target:
            self.target = Target.get(api, self['target'], recurse)

    @memoized_migration
    def migrate(self, api):
        "Naive migrate: Always creates a new contact. Only call as needed."
        path = api.post(TargetContact.base_path, self.reindex(api))
        return TargetContact.get(api, path, True)

    def reindex(self, api):
        result = super(TargetContact, self).reindex(api)
        if self.target:
            result.target = Target.migrate(api)
            result['target'] = result.target['resource_uri']
        return result

class TargetOffice(BaseModel):
    base_path = '/rest/v1/targetoffice/'

    def __init__(self, obj):
        super(TargetOffice, self).__init__(obj)
        self.target = None

    def load_members(self, api, recurse=True):
        if not self.target:
            self.target = Target.get(api, self['target'], recurse)

    @memoized_migration
    def migrate(self, api):
        path = api.post(TargetOffice.base_path, self.reindex(api))
        return TargetOffice.get(api, path, True)

    def reindex(self, api):
        result = super(TargetOffice, self).reindex(api)
        if self.target:
            result.target = Target.migrate(api)
            result['target'] = result.target['resource_uri']
        return result

class Template(BaseModel):
    base_path = '/rest/v1/template/'

    def __init__(self, obj):
        super(Template, self).__init__(obj)
        self.templateset = None

    def load_members(self, api, recurse=True):
        if not self.templateset:
            self.templateset = TemplateSet.get(
                api, self['templateset'], recurse)

    @memoized_migration
    def migrate(self, api, new_template=None):
        """
        If already migrated, then the new_template will just be updated from
        the old, not re-created.
        """
        if new_template:
            path = new_template['resource_uri']
            template = self.reindex(api)
            template['resource_uri'] = path
            api.put(template)
            cache.invalidate(api, new_template)
            logger.debug('Updated template at: %s' % path)
        else:
            path = api.post(Template.base_path, self.reindex(api))
            logger.debug('Created template at: %s' % path)
        return Template.get(api, path)

    def reindex(self, api):
        result = super(Template, self).reindex(api)
        # TemplateSet should already be migrated, just get it
        result.templateset = TemplateSet.find(api, self.templateset['name'])
        assert result.templateset
        result['templateset'] = result.templateset['resource_uri']
        # Sometimes the code_hash is unset on the source server, so we have to
        # build it ourselves...
        if not result['code_hash']:
            sha = hashlib.sha256()
            sha.update(result['code'])
            result['code_hash'] = sha.hexdigest()
        return result

class TemplateSet(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/templateset/'

    # Like Page, TemplateSet cannot be deleted from ActionKit.  Migrate
    # attempts to finish any incomplete migration instead.

    def __init__(self, obj):
        super(TemplateSet, self).__init__(obj)
        self.language = None
        self.templates = []

    def load_members(self, api, recurse=True):
        if not self.language:
            self.language = Language.get(api, self['lang'])
        if not self.templates:
            for path in self['templates']:
                self.templates.append(Template.get(api, path, recurse))

    @memoized_migration
    def migrate(self, api):
        logger.debug('Migrating template set "%s".' % self['name'])
        templateset = TemplateSet.find(api, self[TemplateSet.unique_key], True)
        if templateset:
            # Migrate any member fields that weren't previously migrated
            path = templateset['resource_uri']
            new_set = self.reindex(api)
            new_set['resource_uri'] = path
            api.put(new_set)
            cache.invalidate(api, templateset)
            logger.debug('Updated template set at: %s' % path)
        else:
            path = api.post(TemplateSet.base_path, self.reindex(api))
            logger.debug('Created template set at: %s' % path)
        new_set = TemplateSet.get(api, path, recurse=True)
        try:
            # Filename uniquely identifies a template within a set
            new_templates = new_set.templates
            new_set.templates = []
            new_set['templates'] = []
            for old_template in self.templates:
                for new_template in new_templates:
                    if old_template['filename'] == new_template['filename']:
                        new_template = old_template.migrate(api, new_template)
                        break
                else:
                    new_template = old_template.migrate(api)
                new_set.templates.append(new_template)
                new_set['templates'].append(new_template['resource_uri'])
        except Exception,e:
            new_set.show_partial_migration_warning(api)
            logger.error('exception in template migration'+str(e))
            #raise
        return new_set

    def reindex(self, api):
        result = super(TemplateSet, self).reindex(api)
        result.language = self.language.migrate(api)
        result['lang'] = result.language['resource_uri']
        result.templates = []
        result['templates'] = []
        return result

class User(BaseModel):
    unique_key = 'email'
    base_path = '/rest/v1/user/'
    find_param = 'email'

    def __init__(self, obj):
        super(User, self).__init__(obj)
        self.fields = []
        self.language = None
        self.phones = []
        self.subscriptions = []

    def load_members(self, api, recurse=True):
        if not self.fields:
            for name, value in self['fields'].iteritems():
                self.fields.append(AllowedUserField.find(api, name, recurse))
        if not self.language:
            self.language = Language.get(api, self['lang'])
        if not self.phones:
            for path in self['phones']:
                phone = Phone.get(api, path)
                phone.user = self
                self.phones.append(phone)
        if not self.subscriptions:
            self.subscriptions = Subscription.get_list(
                api, self['subscriptions'])
            if recurse:
                for subscription in self.subscriptions:
                    subscription.load_members(api, recurse)

    @memoized_migration
    def migrate(self, api):
        new_user = User.find(api, self[User.unique_key])
        if not new_user:
            new_user = self.reindex(api)
            path = api.post(User.base_path, new_user)
            logger.debug('Created user at: %s' % path)
            new_user = User.get(api, path)
            try:
                for phone in self.phones:
                    if not phone['normalized_phone']:
                        logger.info('Skipping empty/invalid phone entry.')
                        continue
                    new_phone = phone.migrate(api)
                    new_user.phones.append(new_phone)
                    new_user['phones'].append(new_phone['resource_uri'])
            except Exception,e:
                new_user.show_partial_migration_warning(api)
                logger.error('exception in user migration'+str(e))
                #raise
        return new_user

    def reindex(self, api):
        "Migrate dependencies and return resulting reindexed object."
        result = super(User, self).reindex(api)
        result.fields = []
        for field in self.fields:
            result.fields.append(field.migrate(api))
        result.language = self.language.migrate(api)
        result['lang'] = result.language['resource_uri']
        result.phones = []
        result['phones'] = []
        # Subscriptions are created when page actions are submitted
        result.subscriptions = []
        to_remove = ['actions', 'events', 'eventsignups', 'location',
                     'orderrecurrings', 'orders', 'rand_id', 'subscriptions',
                     'subscription_status', 'subscriptionhistory', 'token',
                     'usermailings', 'useroriginal']
        for key in to_remove:
            del result[key]
        return result

    @staticmethod
    def validate_subscriptions(source_api, target_api):
        logger.info('Ensuring user subscriptions are correct.')
        source_users = cache.data[source_api.domain].get(User, {})
        target_users = cache.data[target_api.domain].get(User, {})
        to_unsubscribe = []
        for key in source_users.keys():
            if key not in target_users:
                continue
            suser = source_users[key]
            tuser = target_users[key]
            if tuser['subscription_status'] == u'subscribed' and \
               suser['subscription_status'] != u'subscribed':
                to_unsubscribe.append(tuser['email'])
        # Do the unsubscribes
        if to_unsubscribe:
            unsubscribe_page = Page.find(target_api, 'unsubscribe', True)
            assert unsubscribe_page, "Can't find unsubscribe page."
            email_enabled = unsubscribe_page.followup['send_email']
            if email_enabled:
                unsubscribe_page.followup.enable_email(target_api, False)
            try:
                for email in to_unsubscribe:
                    logger.info("%s shouldn't be subscribed, unsubscribing..." %
                                email)
                    data = {
                        'page': 'unsubscribe',
                        'email': email,
                    }
                    target_api.post('/rest/v1/action/', data)
            finally:
                if email_enabled:
                    unsubscribe_page.followup.enable_email(target_api, True)

class ReportCategory(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/reportcategory/'
    find_param = 'name'

    def __init__(self, obj):
        super(ReportCategory, self).__init__(obj)

class QueryTemplate(BaseModel):
    unique_key = 'name'
    base_path = '/rest/v1/querytemplate/'
    find_param = 'name'

    def __init__(self, obj):
        super(QueryTemplate, self).__init__(obj)
        self.template = None

class Report(BaseModel):
    unique_key = 'short_name'
    base_path = '/rest/v1/report/'

    def __init__(self, obj):
        super(Report, self).__init__(obj)
        self.categories = []

    def load_members(self, api, recurse=True):
        print "load_members",self.name

    def post_get_hook(self, api, path):
        """
        Just like Pages, Reports come in several kinds and behave differently if they are fetched
        with /report/X or /kindreport/X.  In the latter form, more attributes are
        returned, specific to the kind of report it is.  Therefore, every time a
        report is retrieved with the generic /report/ form, we need to update it
        with the fields retrieved from the report's canonical resource_uri.
        """
        if path != self['resource_uri']:
            logger.debug('Fetching additional report info: %s' %
                         self['resource_uri'])
            obj = api.get(self['resource_uri'])
            if obj:
                self.update(obj)

    @staticmethod
    def find_by_category(api, category):
        "Return a list of reports in category, calling post_get_hook for additional info on each."
        params = {
            'categories__name': category,
        }

        d = {}
        reports_map = map(Report, api.get_list_all('/rest/v1/report/', params=params))
        for report in reports_map:
            d[report['name']] = report
        reports = []
        for report in d.values():
            report.post_get_hook(api, Report.base_path)
            reports.append(cache.find_or_update(api, report))
        logger.info('Found %d reports matching %s.' % (len(reports), category))
        return reports

    def reindex(self, api):
        result = super(Report, self).reindex(api)

        #copy report categories and query templates
        #result['categories'] = []
        #for category in self.categories:
        #    new_category = ReportCategory.find(api,category)
        #    result.categories.append(new_category)
        #    result['categories'].append(new_category['resource_uri'])
        #
        #result['display_as'] = [] #QueryTemplates
        #new_querytemplate = QueryTemplate.find(api,self['display_as'])
        #if new_querytemplate:
        #    result['display_as'] = new_querytemplate['resource_uri']

        #not working yet
        #fake it
        result['categories'] = []
        result['display_as'] = '/rest/v1/querytemplate/1/' #use the standard

        return result

    @memoized_migration
    def migrate(self, api):
        new_report = Report.find(api, self[Report.unique_key])
        if new_report and ('resource_uri' in new_report):
            path = new_report['resource_uri']
            new_report = self.reindex(api)
            new_report['type'] = new_report['type'].lower() #not sure why it won't accept upper case...
            new_report['resource_uri'] = path
            path = api.put(new_report)
            logger.debug('Updated report at: %s' % path)
            if path:
                new_report = Report.get(api, path)
            else:
                logger.error('put error: %s' % new_report[Report.unique_key])
                new_report = None
        else:
            new_report = self.reindex(api)
            new_report['type'] = new_report['type'].lower() #whatever...
            if new_report['type'] == 'dashboard':
                base_path = DashboardReport.base_path

                #also don't post things AK won't take
                new_report['send_if_no_rows'] = ''
                new_report['run_every'] = ''

                #or may be different on the new instance
                new_report['to_emails'] = ''
            elif new_report['type'] == 'query':
                base_path = QueryReport.base_path
            else:
                print "other type:",new_report['type']
                print "THIS MAY NOT WORK"
                return None
                #base_path = Report.base_path
                #if you create a Report, but not it's child object, it can leave the admin in an odd state
            path = api.post(base_path, new_report)
            print "posted",path
            logger.debug('Created report at: %s' % path)
            try:
                new_report = Report.get(api, path)
            except:
                logger.error('get error: %s' % path)
                print "check",path
        return new_report

class QueryReport(Report):
    base_path = '/rest/v1/queryreport/'

class DashboardReport(Report):
    base_path = '/rest/v1/dashboardreport/'
