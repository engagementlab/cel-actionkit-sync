#!/usr/bin/env python

"""
Utility for testing GET requests to the REST API.  Connects to the API and
gets the requested path, displaying the result as pretty-printed JSON.

Example: ./get_request.py -i engagementlab -u user -p pass /user/1/
"""

import optparse
import os
from pprint import pprint
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from migration import Api

class UsageError(Exception): pass

def parse_options(argv):
    parser = optparse.OptionParser()
    parser.add_option('-i', '--instance',
        help='Name of the ActionKit instance (e.g. "engagementlab")')
    parser.add_option('-u', '--username',
        help='Username for API authentication.')
    parser.add_option('-p', '--password',
        help='Password for API authentication.')
    required = ['instance', 'username', 'password']
    options, args = parser.parse_args(argv)
    for r in required:
        if options.__dict__[r] == None:
            parser.error('The following options are required:\n    %s' %
                         '\n    '.join(required))
            raise UsageError()
    if len(args) < 2:
        parser.error('Please specify the path to test, e.g. "/user/schema/".')
        raise UsageError()
    return options, args

def main(argv):
    try:
        options, args = parse_options(argv)
        a = Api(options.instance, options.username, options.password)
        result = a.get('/rest/v1' + args[1])
        pprint(result)
        return 0
    except UsageError:
        return 1

if __name__ == '__main__':
    sys.exit(main(sys.argv))
