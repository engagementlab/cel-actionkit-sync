#!/usr/bin/env python

from datetime import datetime
import optparse
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'

class UsageError(Exception): pass

def migrate(source, target, username, password, cache_file, target_map,
            label, dry_run, force):
    from migration import Migration
    from migration.models import Target
    Target.target_map_file = target_map
    Target.load_target_map()
    m = Migration(source, target, username, password, cache_file)
    m.migrate_pages(label, dry_run=dry_run, force=force)

def parse_options(argv):
    parser = optparse.OptionParser()
    parser.add_option('-s', '--source',
        help='Name of the source instance (e.g. "engagementlab")')
    parser.add_option('-t', '--target',
        help='Name of the target instance (e.g. "forecastthefacts")')
    parser.add_option('-u', '--username',
        help='Username for API authentication.')
    parser.add_option('-p', '--password',
        help='Password for API authentication.')
    parser.add_option('-l', '--label',
        help='Label that identifies items needing migration (e.g.  "climate")')
    parser.add_option('-m', '--target-map',
        help='Map file for target migration.')
    parser.add_option('-n', '--dry-run', action='store_true', default=False,
        help='Simulate the migration - no records will be created.')
    parser.add_option('-f', '--force', action='store_true', default=False,
        help='Re-migrate pages that were migrated before.')
    parser.add_option('-c', '--cache-file',
        help='Initialize the cache with the given file.')
    required = ['source', 'target', 'username', 'password', 'label',
                'target_map']
    options, args = parser.parse_args(argv)
    for r in required:
        if options.__dict__[r] == None:
            parser.error('The following options are required:\n    --%s' %
                         '\n    --'.join(required))
            raise UsageError()
    return options, args

def main(argv):
    try:
        options, args = parse_options(argv)
        migrate(options.source, options.target, options.username,
                options.password, options.cache_file, options.target_map,
                options.label, options.dry_run, options.force)
        return 0
    except UsageError:
        return 1

if __name__ == '__main__':
    sys.exit(main(sys.argv))
