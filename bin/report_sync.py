#!/usr/bin/env python

from datetime import datetime
import optparse
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'

class UsageError(Exception): pass

def sync(instance, username, password,
            label, action, cache_file, source):
    from migration.reports import ReportSync
    rs = ReportSync(instance, username, password, cache_file)
    if action == "dump":
        rs.dump(label)
    elif action == "load":
        rs.load()

def parse_options(argv):
    parser = optparse.OptionParser()
    parser.add_option('-i', '--instance',
        help='Name of the instance (e.g. "engagementlab")')
    parser.add_option('-s', '--source',
        help='Name of the source instance')
    parser.add_option('-u', '--username',
        help='Username for API authentication.')
    parser.add_option('-p', '--password',
        help='Password for API authentication.')
    parser.add_option('-l', '--label',
        help='Label that identifies reports needing migration')
    parser.add_option('-a', '--action',
        help='dump or load')
    parser.add_option('-c', '--cache-file',
        help='Initialize the cache with the given file.')
    parser.add_option('-v', '--verbose',
        action="store_true", dest="verbose",
        help='Write log messages to console')
    required = ['instance', 'username', 'password', 'label', 'action']
    options, args = parser.parse_args(argv)
    for r in required:
        if options.__dict__[r] == None:
            parser.error('The following options are required:\n    --%s' %
                         '\n    --'.join(required))
            raise UsageError()
    if options.action not in ['dump','load']:
        parser.error('action must be one of "dump" or "load"\n')
        raise UsageError()
    if options.action == 'load' and not options.cache_file:
        parser.error('--cache-file required if loading\n')
        raise UsageError()
    return options, args

def main(argv):
    try:
        options, args = parse_options(argv)
        if options.verbose:
            from migration import log
            log.init_console()
        sync(options.instance, options.username, options.password,
            options.label, options.action, options.cache_file, options.source)
        return 0
    except UsageError:
        return 1

if __name__ == '__main__':
    sys.exit(main(sys.argv))
