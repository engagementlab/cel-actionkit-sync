#!/usr/bin/env python

from datetime import datetime
import optparse
import os
import pickle
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

class UsageError(Exception): pass

def build_map(source, target, username, password, output):
    from migration.api import Api
    from migration.models import Target
    source_api = Api(source, username, password)
    target_api = Api(target, username, password)
    print('Building target map...')
    Target.build_target_map(source_api, target_api)
    Target.target_map_file = output
    Target.save_target_map()
    print('Target map written to %s.' % output)

def parse_options(argv):
    parser = optparse.OptionParser()
    parser.add_option('-s', '--source',
        help='Name of the source instance (e.g. "engagementlab")')
    parser.add_option('-t', '--target',
        help='Name of the target instance (e.g. "forecastthefacts")')
    parser.add_option('-u', '--username',
        help='Username for API authentication.')
    parser.add_option('-p', '--password',
        help='Password for API authentication.')
    parser.add_option('-o', '--output',
        help='Target map file to write.')
    required = ['source', 'target', 'username', 'password', 'output']
    options, args = parser.parse_args(argv)
    for r in required:
        if options.__dict__[r] == None:
            parser.error('The following options are required:\n    --%s' %
                         '\n    --'.join(required))
            raise UsageError()
    return options, args

def main(argv):
    try:
        options, args = parse_options(argv)
        build_map(options.source, options.target, options.username,
                options.password, options.output)
        return 0
    except UsageError:
        return 1

if __name__ == '__main__':
    sys.exit(main(sys.argv))
