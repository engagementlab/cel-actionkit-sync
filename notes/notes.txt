How to run the migration script:

First, build the target map (done)
bin/build_target_map.py --source engagementlab --target forecastthefacts --user api_dude --password XXXX --output climate_targets.map

Once a target map exists, run the migration:
bin/migrate.py --source engagementlab --target forecastthefacts --user api_dude --password XXXX --label climate --target-map=climate_targets.map --force

If you want to save time, you can pass --cache-file cache.dat to the migration, and it will load from disk instead of the network. However, I've been having trouble unpickling the cache.dat, and it still uses the same amount of ram, which slows the machine down.